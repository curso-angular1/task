import { AbstractControl, ValidationErrors } from '@angular/forms';

export class UsernameValidator {
    static espaciosblancos(control: AbstractControl) : ValidationErrors | null {
        if((control.value as string).indexOf(' ') >= 0){
            return {espaciosblancos: true}
        }
        return null;
    }
    static nombreValidator(control: AbstractControl) : ValidationErrors | null {
      if((control.value as string).charAt(0) === ' '){
          return {nombreValidator: true}
      }
      return null;
  }
}
