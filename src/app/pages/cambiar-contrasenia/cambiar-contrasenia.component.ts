import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UsernameValidator } from '../../validators/validator-usuario';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-cambiar-contrasenia',
  templateUrl: './cambiar-contrasenia.component.html',
  styleUrls: ['./cambiar-contrasenia.component.css'],
  providers: [AuthService]
})
export class CambiarContraseniaComponent implements OnInit {

  public newPasswordForm: FormGroup;

  constructor(
    private _authService: AuthService,
    private _activatedRoute: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private _formBuilder: FormBuilder,
    private _router: Router


  ) { }

  ngOnInit(): void {
    let token = this._activatedRoute.snapshot.params.token;
    this.newPasswordForm = this._formBuilder.group({
      resettoken: [token],
      password: ['', [Validators.required, Validators.minLength(5), UsernameValidator.espaciosblancos]],
      newpassword: ['', [Validators.required, Validators.minLength(5), UsernameValidator.espaciosblancos]]
    })
  }

  get password() { return this.newPasswordForm.get('password') }
  get newpassword() { return this.newPasswordForm.get('newpassword') }

  cambiar() {
    let data = this.newPasswordForm.value;
    this._authService.setnewPassword(data).subscribe(resp => {
      let data = resp.message;
      this.openSnackBar(data, 'cerrar');
      this._router.navigateByUrl('');
    }, error => {
      let msg = error.error;
      if (!msg.ok) {
        if (msg.errors) {
          let nuevo_errores = msg.errors.map(item => {
            return item.msg;
          });
          let mostrar = nuevo_errores.join(' * ');
          this.openSnackBar(mostrar, 'cerrar');
        } else {
          this.openSnackBar(msg.message, 'cerrar');

        }

      }
    })
    // let data=this.newPasswordForm.value;
    // this._authService.setnewPassword()

  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 5000,
    });
  }


}
