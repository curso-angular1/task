import { Component, OnInit, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-restablecer-password',
  templateUrl: './restablecer-password.component.html',
  styleUrls: ['./restablecer-password.component.css'],
  providers: [AuthService]

})
export class RestablecerPasswordComponent implements OnInit {

  public emailForm: FormGroup;

  constructor(
    private _renderer: Renderer2,
    private _snackBar: MatSnackBar,
    private _formBuilder: FormBuilder,
    private _authService: AuthService
  ) { }

  ngOnInit(): void {
    this.emailForm = this._formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    })
  }
  get email() { return this.emailForm.get('email') }


  ngAfterViewInit() {
    setTimeout(() => {
      var elem = this._renderer.selectRootElement('#myInput');
      // this._renderer.listen(elem, "focus", () => { console.log('focus') });
      // this._renderer.listen(elem, "blur", () => { console.log('blur') });
      elem.focus();

    }, 500);
  }
  restablecer(formDirective: FormGroupDirective) {

    if (this.emailForm.valid) {
      let data = this.emailForm.value;
      this._authService.resetPassword(data.email).subscribe(resp => {
        formDirective.resetForm()
        this.email.reset();
        this.openSnackBar('Se ha enviado un correo', 'cerrar');
      }, error => {
        if (!error.error.ok) {
          let msg = error.error.message;
          this.openSnackBar(msg, 'cerrar')
        }
      })
    }
  }
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 5000,
    });
  }




}
