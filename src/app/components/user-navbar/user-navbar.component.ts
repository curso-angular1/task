import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { UsuarioService } from '../../services/usuario.service';
import { usuario } from '../../models/usuario';
@Component({
  selector: 'app-user-navbar',
  templateUrl: './user-navbar.component.html',
  styleUrls: ['./user-navbar.component.css'],
  providers: [AuthService, UsuarioService]

})
export class UserNavbarComponent implements OnInit {

  public usuario: usuario;

  constructor(
    private _authService: AuthService,
    private _usuarioService: UsuarioService
  ) {
    this.usuario = new usuario('', '','', '', null, false, '');
  }

  ngOnInit(): void {
    this.get_user();
  }

   get_user() {
     this._usuarioService.get_usuario().subscribe(resp => {
      this.usuario = resp.usuario;
    });
  }


  logout() {
    this._authService.logout()
  }

}
