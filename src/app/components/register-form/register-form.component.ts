import { Component, OnInit } from '@angular/core';
import { usuario } from '../../models/usuario'
import { Validators, FormGroup, FormBuilder, FormGroupDirective } from '@angular/forms';
import { UsernameValidator } from '../../validators/validator-usuario';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css'],
  providers: [AuthService]

})
export class RegisterFormComponent implements OnInit {

  public nuevo_usuario: usuario;
  public usuarioForm: FormGroup;

  constructor(
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private _usuarioService: AuthService
  ) { }

  ngOnInit(): void {
    this.usuarioForm = this.formBuilder.group({
      nombre: ['', [Validators.required, UsernameValidator.nombreValidator]],
      apellidos: ['', [Validators.required, UsernameValidator.nombreValidator]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(5), UsernameValidator.espaciosblancos]],
    });
  }

  get nombre() { return this.usuarioForm.get('nombre'); }
  get apellidos() { return this.usuarioForm.get('apellidos'); }
  get email() { return this.usuarioForm.get('email'); }
  get password() { return this.usuarioForm.get('password'); }


  registrarUsuario(formData: any, formDirective: FormGroupDirective) {
    if (this.usuarioForm.valid) {
      this.nuevo_usuario = this.usuarioForm.value;
      this._usuarioService.registrarUsuario(this.nuevo_usuario).subscribe(resp => {
        if (resp['ok']) {
          this.openSnackBar('El usuario se creo con exito', 'cerrar');
          formDirective.resetForm({ nombre: '', apellidos: '', email: '', password: '' });
          this.usuarioForm.reset({ nombre: '', apellidos: '', email: '', password: '' });
        }

      }, (error: any) => {
        let errores = error.error.errors;
        if (errores.length > 0) {
          let nuevo_errores = errores.map(item => {
            return item.msg;
          });
          let mostrar = nuevo_errores.join('\n');
          this.openSnackBar(mostrar, 'cerrar');

        }
      })
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 5000,
    });
  }
}
