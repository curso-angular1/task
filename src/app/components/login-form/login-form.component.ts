import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { UsernameValidator } from '../../validators/validator-usuario';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css'],
  providers: [AuthService]

})
export class LoginFormComponent implements OnInit {

  public usuarioLoginForm: FormGroup;

  constructor(
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private _loginService: AuthService,
    private _router: Router

  ) {
  }

  ngOnInit(): void {
    this.usuarioLoginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(5), UsernameValidator.espaciosblancos]],
    });
  }

  get email() { return this.usuarioLoginForm.get('email') }
  get password() { return this.usuarioLoginForm.get('password') }

  loginUser() {
    this._loginService.signIn(this.usuarioLoginForm.value)
      .subscribe((res: any) => {
        this._router.navigateByUrl('home');
      }, (error: any) => {
        let msg = error.error.message;
        this.openSnackBar(msg, 'cerrar')
      })
  }
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 5000,
    });
  }

}
