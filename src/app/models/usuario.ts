export class usuario {

  constructor(
    public _id: string,
    public nombre: string,
    public apellidos: string,
    public email: string,
    public img: string,
    public estado:boolean,
    public password?: string) {
  }

}
