import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs'
import { usuario } from '../models/usuario'
import { url_dominio } from '../services/global';

@Injectable()
export class UsuarioService {

  public url: string

  constructor(private _http: HttpClient) {
    this.url = url_dominio
  }

  public get_usuario(): Observable<any> {
    return this._http.get(`${this.url}/get-user`);
  }



}
