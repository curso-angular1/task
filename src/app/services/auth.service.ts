import { Injectable } from '@angular/core';
import { usuario } from '../models/usuario'
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { url_dominio } from './global';
import { Router, ActivatedRoute, Params } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public url: string;
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  currentUser = {};


  constructor(
    private _http: HttpClient,
    private _router: Router
  ) {
    this.url = url_dominio;
  }

  public registrarUsuario(usuario):Observable<any> {
    let params = JSON.stringify(usuario);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this._http.post(`${this.url}/crear-usuario`, params, { headers });
  }

  public signIn(user: usuario): Observable<any> {
    return this._http.post<any>(`${this.url}/login`, user).pipe(
      tap(
        (resp: any) => {
          this.saveToken(resp.token, resp.expiresIn);
        }
      )
    )
  }
  public logout(): void {
    localStorage.removeItem("ACCESS_TOKEN");
    localStorage.removeItem("EXPIRESIN");
    this._router.navigateByUrl('');
  }

  private saveToken(token: string, expiresIn: string): void {
    localStorage.setItem("ACCESS_TOKEN", token);
    localStorage.setItem("EXPIRESIN", expiresIn);
  }
  public getToken() {
    return localStorage.getItem('ACCESS_TOKEN');
  }
  public isAuthenticated(): boolean {
    return (localStorage.getItem('ACCESS_TOKEN') !== null) ? true : false;
  }

  public resetPassword(email: string):Observable<any> {
    return this._http.post<any>(`${this.url}/restablecer-password`, { email });
  }
  public setnewPassword(data: any): Observable<any> {
    return this._http.post(`${this.url}/nuevo-password`, data);
  }


}
