import { ModuleWithProviders } from '@angular/core'
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { AuthGuard } from './services/auth.guard';
import { GuestGuard } from './services/guest.guard';
import { RestablecerPasswordComponent } from './pages/restablecer-password/restablecer-password.component';
import { CambiarContraseniaComponent } from './pages/cambiar-contrasenia/cambiar-contrasenia.component';
import { SidenavMenuComponent } from './pages/sidenav-menu/sidenav-menu.component';
import { MisProyectosComponent } from './components/mis-proyectos/mis-proyectos.component';
import { CrearProyectoComponent } from './components/crear-proyecto/crear-proyecto.component';
import { ErrorComponent } from './pages/error/error.component';
const routes: Routes = [
  { path: '', component: LoginComponent, canActivate: [GuestGuard] },
  {
    path: 'home', component: SidenavMenuComponent, canActivate: [AuthGuard],
    children: [
      { path: '', component: MisProyectosComponent, outlet: "sidebar" },
      { path: 'crear-proyecto', component: CrearProyectoComponent, outlet: "sidebar" },

    ]
  },
  { path: 'restablecer', component: RestablecerPasswordComponent, canActivate: [GuestGuard] },
  { path: 'cambiar-contrasenia/:token', component: CambiarContraseniaComponent, canActivate: [GuestGuard] },



  { path: '**', component: ErrorComponent }
]

export const appRoutingProvider: any = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(routes);

